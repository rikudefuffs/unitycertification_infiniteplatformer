﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The base class for all enemies of this game
/// </summary>
public class Enemy : MonoBehaviour, IDamageable, ISpawnable
{
    public int maxHealth = 1;
    public int health;
    [SerializeField]
    internal SpriteRenderer spriteRenderer;

    [SerializeField]
    internal Rigidbody2D myRigidbody2D;
    internal bool isDead = false;
    public GameObject Spawn(Vector3 position)
    {
        Enemy instance = Instantiate(gameObject, position, Quaternion.identity).GetComponent<Enemy>();
        instance.Setup();
        return instance.gameObject;
    }

    internal virtual void Setup()
    {
        health = maxHealth;
        isDead = false;
    }

    public void Die()
    {
        isDead = true;
        Destroy(gameObject);
    }

    public void TakeDamage(int amount, IDamageSource damageSource)
    {
        health -= amount;
        if (health < 1)
        {
            damageSource.OnEnemyKilled(this);
            EnemyManager.Singleton.OnEnemyDied(this);
            Die();
        }
    }

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (isDead) { return; }

        PlayerCharacter player = collision.collider.gameObject.GetComponent<PlayerCharacter>();
        if (!player) { return; }
        
        Vector3 playerPos = collision.collider.bounds.center;
        float playerHeight = collision.collider.bounds.extents.y;
        bool playerIsAboveEnemy = (playerPos.y - playerHeight) > transform.position.y;

        if (playerIsAboveEnemy)
        {
            TakeDamage(maxHealth, player);
            return;
        }
        player.KillCharacter();
    }
}


public interface ISpawnable
{
    GameObject Spawn(Vector3 position);
}

public interface IDamageSource
{
    void OnEnemyKilled(Enemy enemyKilled);
}

public interface IDamageable
{
    void TakeDamage(int amount, IDamageSource damageSource);
    void Die();
}
