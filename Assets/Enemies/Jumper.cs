﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumper : GroundEnemy
{
    [SerializeField]
    int jumpHeight = 10;

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (isDead) { return; }
        if (!collision.gameObject.CompareTag(GameplayConstants.TAG_Ground)) { return; }
        myRigidbody2D.AddForce(Vector3.up * jumpHeight, ForceMode2D.Impulse);
    }
}
