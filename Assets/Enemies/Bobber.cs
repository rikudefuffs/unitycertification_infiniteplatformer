﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bobber : AirEnemy
{
    [SerializeField]
    float maxHeightAboveStartingPoint;
    Vector3 lowPoint;
    Vector3 highPoint;

    [SerializeField]
    float speed = 1;
    float startTime;
    bool goUp;

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        return;
    }

    internal override void Setup()
    {
        base.Setup();
        lowPoint = transform.position;
        highPoint = lowPoint + Vector3.up * maxHeightAboveStartingPoint;
        startTime = Time.time;
        goUp = true;
    }

    void Update()
    {
        float distanceCovered = (Time.time - startTime) * speed;
        float fractionOfJourney = distanceCovered / maxHeightAboveStartingPoint;

        if (goUp)
        {
            transform.position = Vector3.Lerp(lowPoint, highPoint, fractionOfJourney);
        }
        else
        {
            transform.position = Vector3.Lerp(highPoint, lowPoint, fractionOfJourney);
        }

        if (fractionOfJourney >= 1)
        {
            goUp = !goUp;
            startTime = Time.time;
        }
    }
}
