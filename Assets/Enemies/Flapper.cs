﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flapper : AirEnemy
{
    [SerializeField]
    int jumpHeight = 10;
    float jumpHeightThreshold;

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < jumpHeightThreshold)
        {
            myRigidbody2D.AddForce(Vector3.up * jumpHeight, ForceMode2D.Force);
        }
    }
}
