﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walker : GroundEnemy
{
    public int maxSpeed = 1;
    public int speed;
    Vector2 leftDirection = Vector2.left;
    Vector2 rightDirection = Vector2.right;
    Vector2 currentDirection;
    bool canMove;

    [SerializeField]
    LayerMask obstacleMask;                  

    internal override void Setup()
    {
        base.Setup();
        speed = maxSpeed;
        currentDirection = leftDirection; 
    }

    void Update()
    {
        //if i'd fall by moving
        Vector3 fallMovementDirection = new Vector2(currentDirection.x * 0.1f, 0); //Debug.DrawRay(transform.position + movementDirection, Vector2.down * 1, Color.black);
        if (!Physics2D.Raycast(transform.position + fallMovementDirection, Vector2.down, 1, obstacleMask))
        {
            ChangeDirection();
            return;
        }

        //if i'm hitting a wall in the direction i'm facing
        Vector2 checkMovementDirection = new Vector2(currentDirection.x, 0).normalized; //Debug.DrawRay(transform.position, checkMovementDirection * 0.5f, Color.red);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, checkMovementDirection, 0.5f, obstacleMask);
        if (hit)
        {
            ChangeDirection();
            return;
        }
        myRigidbody2D.velocity = new Vector2(currentDirection.x * speed, myRigidbody2D.velocity.y);
    }

    void ChangeDirection()
    {
        currentDirection = (currentDirection == leftDirection) ? rightDirection : leftDirection;
    }
}
