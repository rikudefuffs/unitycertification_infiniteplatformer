﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public delegate void OnAliveEnemiesChangedHandler(int newEnemiesCount);
    public static event OnAliveEnemiesChangedHandler OnAliveEnemiesChanged;

    public static EnemyManager Singleton { get; private set; }
    public List<GroundEnemy> groundEnemies = new List<GroundEnemy>();
    public List<AirEnemy> airEnemies = new List<AirEnemy>();
    int aliveEnemies = 0;

    void Awake()
    {
        if (Singleton && Singleton != this)
        {
            Destroy(gameObject);
            return;
        }
        Singleton = this;
    }


    void Start()
    {
        OnAliveEnemiesChanged?.Invoke(aliveEnemies);
    }

    public void OnEnemyDied(Enemy enemy)
    {
        aliveEnemies--;
        OnAliveEnemiesChanged?.Invoke(aliveEnemies);
    }

    public void SpawnEnemiesForPlatform(PlatformSection platform)
    {
        PlatformSection.PositionInLevel[] positionsInLevel = platform.GetEnemySpawnPoints(3);
        if (positionsInLevel == null) { return; }
        try
        {
            foreach (var spawnPoint in positionsInLevel)
            {
                if (spawnPoint.isOnGround)
                {
                    groundEnemies[Random.Range(0, groundEnemies.Count)].Spawn(spawnPoint.position);
                }
                else
                {
                    airEnemies[Random.Range(0, airEnemies.Count)].Spawn(spawnPoint.position);
                }
                aliveEnemies++;
                OnAliveEnemiesChanged?.Invoke(aliveEnemies);
            }

        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex.Message);
        }
    }

    public void SpawnEnemiesBetweenPlatforms(PlatformSection firstPlatform, PlatformSection otherPlatform)
    {
        Vector3 pointBetweenPlatforms = (otherPlatform.center + firstPlatform.center) * 0.5f;
        bool spaceBetweenPlatformsExists = !Physics2D.Raycast(pointBetweenPlatforms + (Vector3.up * 5), Vector2.down, 10, 1 << GameplayConstants.LAYER_Ground);
        Debug.DrawRay(pointBetweenPlatforms, Vector2.down * 10, Color.yellow, 100);
        if (!spaceBetweenPlatformsExists) { return; }

        airEnemies[Random.Range(0, airEnemies.Count)].Spawn(pointBetweenPlatforms);
        aliveEnemies++;
        OnAliveEnemiesChanged?.Invoke(aliveEnemies);
    }
}
