﻿using System.Collections.Generic;
using UnityEngine;

public class LocalizationManager : MonoBehaviour
{
    public static LocalizationManager instance;

    public enum Language
    {
        English,
        Italian
    }

    Dictionary<string, Dictionary<string, string>> localizationLibrary;

    public Language currentLanguage;

    void Awake()
    {
        if (instance && instance != this)
        {
            Destroy(this);
            return;
        }
        instance = this;
        Initialize();
    }

    void Initialize()
    {
        localizationLibrary = new Dictionary<string, Dictionary<string, string>>();

        foreach (var localizationFile in Resources.LoadAll("Languages", typeof(TextAsset)))
        {
            localizationLibrary.Add(localizationFile.name, LoadDictionaryFromLanguageFile(localizationFile as TextAsset));
        }
    }

    Dictionary<string, string> LoadDictionaryFromLanguageFile(TextAsset languageFile)
    {
        Dictionary<string, string> outputDictionary = new Dictionary<string, string>();
        string fileText = languageFile.text;

        string[] lines = fileText.Split('\n');
        foreach (string line in lines)
        {
            string[] values = line.Split(',');
            outputDictionary.Add(values[0], values[1].Replace("\r", ""));
        }

        return outputDictionary;
    }

    public string Get(string key)
    {
        string result;
        if (localizationLibrary[currentLanguage.ToString()].TryGetValue(key, out result))
        {
            return result;
        }
        Debug.LogWarningFormat("'{0}' key not found for language {1}", key, currentLanguage.ToString());
        return string.Empty;
    }
}