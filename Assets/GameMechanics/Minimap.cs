﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{
    public static Minimap instance;
    List<MinimapToken> tokens;

    [SerializeField]
    MinimapToken tokenPrefab;

    [SerializeField]
    MapTarget mapTarget;

    void Awake()
    {
        if (instance && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        tokens = new List<MinimapToken>();
    }

    public void AddTokenFor(MinimapTokenTracker tokenTracker)
    {
        MinimapToken newToken = Instantiate(tokenPrefab, transform);
        newToken.Initialize(tokenTracker);
        tokens.Add(newToken);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        UpdateTokensPosition();
    }

    void UpdateTokensPosition()
    {
        Vector2 centerOfMinimap = mapTarget.transform.position;
        float halfOfLookAheadDistance = mapTarget.lookAheadDistance / 4;
        MinimapToken token;
        for (int i = tokens.Count - 1; i >= 0; i--)
        {
            token = tokens[i];
            if (!token.IsValid)
            {
                tokens.RemoveAt(i);
                Destroy(token.gameObject);
                continue;
            }
            Vector2 positionInWorld = token.GetPosition();
            Vector2 positionOnMinimap = new Vector2(positionInWorld.x - centerOfMinimap.x, positionInWorld.y - centerOfMinimap.y);
            token.transform.localPosition = positionOnMinimap * halfOfLookAheadDistance;
        }
    }
}
