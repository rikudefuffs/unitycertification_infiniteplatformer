﻿using UnityEngine;
using UnityStandardAssets._2D;

public class PlayerCharacter : MonoBehaviour, IDamageSource
{
    public static PlayerCharacter Singleton { get; set; }

    public delegate void OnLivesChangedHandler(int newLives);
    public static event OnLivesChangedHandler OnLivesChanged;

    public delegate void OnScoreChangedHandler(int newScore);
    public static event OnScoreChangedHandler OnScoreChanged;

    internal Rigidbody2D rb;

    [SerializeField]
    internal PlatformerCharacter2D platformCharacter;

    int lives = GameplayConstants.STARTING_LIVES;
    int distanceScore = 0;
    int enemyScore = 0;

    void Awake()
    {
        if (Singleton && Singleton != this)
        {
            Destroy(gameObject);
            return;
        }
        Singleton = this;
    }

    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        OnLivesChanged?.Invoke(lives);
    }

    void Update()
    {
        distanceScore = Mathf.Max(distanceScore, (int)this.transform.position.x);
        int totalScore = distanceScore * GameplayConstants.SCORE_DISTANCE_MULTIPLIER + enemyScore * GameplayConstants.SCORE_ENEMY_MULTIPLIER;
        OnScoreChanged?.Invoke(totalScore);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == GameplayConstants.TAG_KillZone)
        {
            KillCharacter();
        }
    }

    public void OnEnemyKilled(Enemy enemyKilled)
    {
        //todo you could track different stats based on the enemy killed
        enemyScore++;
    }

    public void KillCharacter()
    {
        lives -= 1;
        OnLivesChanged?.Invoke(lives);
        if (lives > 0)
        {
            rb.MovePosition(rb.position + GameplayConstants.RESPAWN_HEIGHT * Vector2.up);
            rb.velocity = Vector2.zero;
        }
        else
        {
            GameOver();
        }
    }

    void GameOver()
    {
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;
        Debug.Log("Game Over!");
    }
}
