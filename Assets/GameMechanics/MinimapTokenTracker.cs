﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapTokenTracker : MonoBehaviour
{
    public enum MinimapTokenType
    {
        Player,
        Enemy
    }

    public MinimapTokenType tokenType;

    void Start()
    {
        Minimap.instance.AddTokenFor(this);
    }
}
