﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTarget : MonoBehaviour
{
    public Transform targetToFollow;
    public float lookAheadDistance = 10;
    public float elevation = 0;

    void LateUpdate()
    {
        Vector3 updatePosition = targetToFollow.position;
        updatePosition.x += lookAheadDistance;
        updatePosition.y = elevation;
        transform.position = updatePosition;
    }
}
