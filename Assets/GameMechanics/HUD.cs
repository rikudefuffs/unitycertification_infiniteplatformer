﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class HUD : MonoBehaviour
{
    public TMP_Text lblScore;
    public TMP_Text lblEnemies;
    public TMP_Text lblLives;


    #region Localization
    [SerializeField]
    LocalizationManager.Language currentLanguage;

    int cachedLives;
    int cachedEnemies;
    int cachedScore;

    #endregion

    #region Dev

    public TMP_Text lblVelocity;
    public TMP_Text lblUpVelocity;
    public TMP_Text lblIsOnGround;

    public void UpdateVelocity(float newValue)
    {
        UpdateLabel(lblVelocity, "Horizontal Velocity", newValue);
    }

    public void UpdateVerticalVelocity(float newValue)
    {
        UpdateLabel(lblUpVelocity, "Vertical Velocity", newValue);
    }

    public void UpdateIsOnGround(bool newValue)
    {
        UpdateLabel(lblIsOnGround, "Is on ground", newValue);
    }

    public void UpdateLabel(TMP_Text label, string baseText, object newValue)
    {
        label.text = string.Format("{0}: {1}", baseText, newValue);
    }

    #endregion

    public int GenerateLayerMask(int[] layers)
    {
        int mask = 0;
        foreach (var layer in layers)
        {
            mask |= 1 << layer;
        }
        return mask;
    }

    public int InvertLayerMask(int[] layers)
    {
        return ~GenerateLayerMask(layers);
    }

    void Awake()
    {
        PlayerCharacter.OnLivesChanged += OnLivesChanged;
        PlayerCharacter.OnScoreChanged += OnScoreChanged;
        EnemyManager.OnAliveEnemiesChanged += OnEnemiesChanged;
    }

    void Start()
    {
        Localize();
    }

    void OnDestroy()
    {
        PlayerCharacter.OnLivesChanged -= OnLivesChanged;
        PlayerCharacter.OnScoreChanged -= OnScoreChanged;
        EnemyManager.OnAliveEnemiesChanged -= OnEnemiesChanged;
    }

    void Update()
    {
#if UNITY_EDITOR
        UpdateVelocity(PlayerCharacter.Singleton.rb.velocity.x);
        UpdateVerticalVelocity(PlayerCharacter.Singleton.rb.velocity.y);
        UpdateIsOnGround(PlayerCharacter.Singleton.platformCharacter.grounded);
#endif
        if (currentLanguage == LocalizationManager.instance.currentLanguage) { return; }
        LocalizationManager.instance.currentLanguage = currentLanguage;
        Localize();
    }

    void Localize()
    {
        OnLivesChanged(cachedLives);
        OnScoreChanged(cachedScore);
        OnEnemiesChanged(cachedEnemies);
    }


    void OnLivesChanged(int newLives)
    {
        cachedLives = newLives;
        lblLives.text = string.Format("{0}: {1}", LocalizationManager.instance.Get("Lives"), newLives);
    }

    void OnScoreChanged(int newScore)
    {
        cachedScore = newScore;
        lblScore.text = string.Format("{0}: {1}", LocalizationManager.instance.Get("Score"), newScore);
    }

    void OnEnemiesChanged(int newEnemiesCount)
    {
        cachedEnemies = newEnemiesCount;
        lblEnemies.text = string.Format("{0}: {1}", LocalizationManager.instance.Get("Enemies"), newEnemiesCount);
    }
}
