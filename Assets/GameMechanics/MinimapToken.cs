﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// A representation of an entity on the minimap
/// </summary>
public class MinimapToken : MonoBehaviour
{
    [SerializeField]
    Image imgToken;

    MinimapTokenTracker objectToTrack;

    public bool IsValid { get { return objectToTrack != null; } }

    public void Initialize(MinimapTokenTracker objectToTrack)
    {
        this.objectToTrack = objectToTrack;

        switch (objectToTrack.tokenType)
        {
            case MinimapTokenTracker.MinimapTokenType.Player:
                imgToken.color = Color.yellow;
                break;
            case MinimapTokenTracker.MinimapTokenType.Enemy:
                imgToken.color = Color.red;
                break;
            default:
                imgToken.color = Color.white;
                break;
        }
    }

    public Vector2 GetPosition()
    {
        return objectToTrack.transform.position;
    }
}
