using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        private PlatformerCharacter2D m_Character;
        private bool m_Jump;



        public AutoRunState autorun = AutoRunState.Disabled;

        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();
        }


        private void Update()
        {
            if (!m_Jump)
            {
                // Read the jump input in Update so button presses aren't missed.
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }
        }


        private void FixedUpdate()
        {
            if (autorun != AutoRunState.Disabled)
            {
                AutoRun();
                return;
            }

            // Read the inputs.
            bool crouch = Input.GetKey(KeyCode.LeftControl);
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            // Pass all parameters to the character control script.
            m_Character.Move(h, crouch, m_Jump);
            m_Jump = false;
        }

        #region AutoRun

        public enum AutoRunState
        {
            Disabled,
            Enabled,
            Verbose
        }

        [Header("AutoRun Settings")]
        public float autoCautiousSpeed = 0.25f;
        public float autoFootprint = 1f;
        public float autoAirborneThreshold = 1f;
        public float autoCliffDetection = 1f;
        public float autoMaxCliff = 1f;
        public float autoObstacleDetection = 1f;
        public float autoObstacleThreshold = 1f;
        public float autoJumpMinTime = 0.5f;
        LayerMask autoMask = (1 << 9) | (1 << 11); //257;

        [Range(0.01f, 1f)]
        public float autoPlaybackSpeed = 1f;
        float jumpTimer = -1f;
        string debugMessage;

        void AutoRun()
        {
            Time.timeScale = autoPlaybackSpeed;
            float h = 1f;
            m_Jump = false;
            bool crouch = false;

            Vector3 currentPosition = transform.position + autoFootprint * Vector3.left;
            jumpTimer -= Time.fixedDeltaTime;

            //Am I airborne?
            RaycastHit2D hit = Physics2D.Raycast(currentPosition, Vector2.down, autoAirborneThreshold, autoMask);
            AutoRunDebug(false);

            if (ColliderExistsAndNotTrigger(hit))
            {
                AutoRunGrounded(ref h, ref currentPosition, ref hit);
            }
            else
            {
                hit = Physics2D.Raycast(currentPosition + 2f * autoFootprint * Vector3.right, Vector2.down, autoAirborneThreshold, autoMask);
                if (ColliderExistsAndNotTrigger(hit))
                {
                    AutoRunGrounded(ref h, ref currentPosition, ref hit);
                }
                else
                {
                    AutoRunAirborne(ref h, ref currentPosition, ref hit);
                }
            }
            m_Character.Move(h, crouch, m_Jump);
            AutoRunDebug(true);
        }


        bool ColliderExistsAndNotTrigger(RaycastHit2D hit)
        {
            return hit.collider && !hit.collider.isTrigger;
        }

        void AutoRunAirborne(ref float horizontalMovement, ref Vector3 currentPosition, ref RaycastHit2D hit)
        {
            AutoRunDebug("\nI'm airborne.");
            if (jumpTimer > 0f)
            {
                AutoRunDebug("\nI just jumped, full speed ahead!");
            }
            else
            {
                hit = Physics2D.Raycast(currentPosition + 2f * autoFootprint * Vector3.right, Vector2.down, Mathf.Infinity, autoMask);
                if (ColliderExistsAndNotTrigger(hit))
                {
                    //land on it!
                    if (hit.collider.CompareTag("Enemy"))
                    {
                        //I'm over an enemy
                        float distance = hit.transform.position.x - transform.position.x;
                        AutoRunDebug(string.Format("\nCan land on enemy: {0}... moving of {1} units", hit.collider.name, distance));
                        horizontalMovement = Mathf.Clamp(distance, -1, 1);
                    }
                    else
                    {
                        AutoRunDebug(string.Format("\nCan land on ground: {0}", hit.collider.name));
                        horizontalMovement = 0;
                    }
                }
                else
                {
                    //just keep jumping
                    AutoRunDebug("\nNothing to land on");
                }
            }
        }

        void AutoRunGrounded(ref float horizontalMovement, ref Vector3 currentPosition, ref RaycastHit2D hit)
        {
            jumpTimer = -1f;

            if (hit.collider.CompareTag("Enemy"))
            {
                //I'm on an enemy
                AutoRunDebug("\nTrying to jump off an enemy!");
                TryToJump();
                return;
            }

            AutoRunDebug("\nI'm on the ground");
            hit = Physics2D.Raycast(currentPosition + autoCliffDetection * Vector3.right, Vector2.down, autoMaxCliff, autoMask);
            if (ColliderExistsAndNotTrigger(hit))
            {
                //Not a cliff. Do I need to jump?
                hit = Physics2D.Raycast(currentPosition + autoObstacleThreshold * Vector3.down, Vector2.right, autoObstacleDetection, autoMask);
                if (ColliderExistsAndNotTrigger(hit))
                {
                    hit = JumpOverObstacle(hit);
                }
                else
                {
                    //no wall
                    AutoRunDebug("\nNo cliffs or walls or enemies, moving cautiously...");
                    horizontalMovement = autoCautiousSpeed;
                }
            }
            else
            {
                //cliff :(
                AutoRunDebug("\nNo cliff ahead detected, trying to jump...");
                TryToJump();
                if (m_Jump)
                {
                    AutoRunDebug("\nI jumped...");
                }
            }
        }

        RaycastHit2D JumpOverObstacle(RaycastHit2D hit)
        {
            AutoRunDebug(string.Format("\nDetected {0}! Attempting to jump...", hit.collider.name));
            TryToJump();
            if (m_Jump)
            {
                AutoRunDebug("\nI jumped!");
            }
            return hit;
        }


        void TryToJump()
        {
            if (m_Jump) { return; }
            m_Jump = true;
            jumpTimer = autoJumpMinTime;
        }

        void AutoRunDebug(string message)
        {
            if (autorun == AutoRunState.Verbose)
            {
                debugMessage += message;
            }
        }

        void AutoRunDebug(bool writeToConsole)
        {
            if (autorun != AutoRunState.Verbose) { return; }
            if (writeToConsole)
            {
                Debug.Log(debugMessage);
            }
            else
            {
                debugMessage = string.Empty; // Time.time.ToString(); //just maintaining compatibility with previous versions of unity
            }
        }

        void OnDrawGizmos()
        {
            if (autorun != AutoRunState.Verbose) { return; }
            Vector3 currentPosition = transform.position + autoFootprint * Vector3.left;

            //airborne check
            Vector3 offset = currentPosition + autoAirborneThreshold * Vector3.down;
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(currentPosition, offset);

            //groudn check
            Gizmos.color = Color.green;
            Gizmos.DrawLine(offset, offset + 1000f * Vector3.down);

            //cliff check
            Gizmos.color = Color.red;
            //Gizmos.DrawLine(currentPosition + autoCliffDetection * Vector3.right, currentPosition + new Vector3(autoCliffDetection);

            //wall check
            currentPosition += autoObstacleDetection * Vector3.down;
            Gizmos.color = Color.white;
            Gizmos.DrawLine(currentPosition, currentPosition + autoObstacleDetection * Vector3.right);

        }
        #endregion
    }
}
